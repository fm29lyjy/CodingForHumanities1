# Homework 1 #
My name is Hannes.

## My Expectations are: ##
  - learning [R](https://de.wikipedia.org/wiki/R_(Programmiersprache))
  - advancing in [Digital Humanitites](http://www.dh.uni-leipzig.de/wo/)
  - advancing in mark-up/-[down](http://markdown.de/)
  - working [collaboratively](https://de.wikipedia.org/wiki/Kollaboration) in a team 

## Some random c-code: ##
```c
#import <stdio.h>
int main()
{
    printf("Hello students of digital humanities!");
    return 0;
}
```