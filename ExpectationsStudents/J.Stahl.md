# Coding for Humanities #

As I was scrolling the possibilities of studying at the University of Leipzig, as I scrumbled upon "Digital Humanities". It sounded really interesting and now I'm giving it a try!

## Expectations; Jonas Stahl ##

1. Learn to code.
2. Use technologies of my age to research Humanities.
3. Have a good time in Leipzig.

[Thumbs up!](http://vignette1.wikia.nocookie.net/unturned-bunker/images/0/0a/Meaning-of-vault-boy-thumbs-up-jpg.jpg/revision/latest?cb=20160316025719 "Thumbs up!")